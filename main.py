#Author: John A. Krol

import argparse
import imageio as iio
import matplotlib.pyplot as img
import Loaded_Image as LI
import time

def main():
    parser = argparse.ArgumentParser(description="This is a description.")
    parser.add_argument('input', metavar="i",
                        help='The input filename. The exension should be included. Example: test.jpg.')
    parser.add_argument('numproj', metavar="p",
                        help='Number of projections to use.')
    parser.add_argument('sangle', metavar="s", help='Start projection angle (from y-axis).')
    parser.add_argument('eangle', metavar="e", help='End projection angle (from y-axis).')
    parser.add_argument('--clibs', action='store_true', help='Significantly reduces computation time by utilizing compiled C libraries.')
    parser.add_argument('--show', action='store_true')
    parser.add_argument('--time', action='store_true', help='Times operation and prints to console. Excludes any file I/O or initialization.')
    parser.add_argument('--filt', help='Filter type. 0=Unfiltered, 1=Ramp, 2=Ramp with Hanning window. Defaults to 2.')
    args = parser.parse_args()
    my_phantom = LI.Loaded_Image(iio.imread(args.input), "input_image", use_clibs=args.clibs)

    if args.time:
        start = time.time()

    my_phantom.create_projection_profile(int(args.numproj), int(args.sangle), int(args.eangle), filter_type=int(args.filt), use_clibs=args.clibs)
    my_phantom.forward_project()
    my_phantom.filter_sinogram()
    my_phantom.back_project()

    if args.time:
        stop = time.time()
        print('Statistics:')
        print('Number of projections: ' + args.numproj)
        print("Time elapsed: " + str(stop - start) + " seconds.")
        print('Avg. computation time: ' + str((stop-start)/float(args.numproj)) + ' seconds/computation.')

    iio.imwrite('int_' + args.input, my_phantom.intensity)
    iio.imwrite('sino_' + args.input, my_phantom.sinogram)
    iio.imwrite('backproj_' + args.input, my_phantom.backprojected)
    import numpy as np

    if args.show:
        img.figure()
        img.imshow(my_phantom.original)
        img.title("Input image")
        img.figure()
        img.imshow(my_phantom.sinogram, cmap='gray')
        img.title("Raw Sinogram")
        img.figure()
        img.imshow(my_phantom.filtered_sinogram, cmap='gray')
        img.title("Filtered Sinogram")
        img.figure()
        img.imshow(my_phantom.backprojected, extent=[0, 1, 0, 1], cmap='gray')
        img.title("Backprojected")
        img.show()

if __name__ == "__main__":
    main()