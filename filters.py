#Author: John A. Krol

import numpy as np

class Filter():

    def __init__(self, length, filter_type=1):
        if filter_type == 0:
            self.filter = 1
        elif filter_type == 1:
            self.filter = self._ramp(length)
        elif filter_type == 2:
            self.filter = self._ramp_with_hanning(length)
        else:
            raise Exception

    def _ramp(self, length):
        return np.concatenate((np.arange(0,1,2/length), np.arange(1,0,-2/length)))

    def _ramp_with_hanning(self, length):
        n = np.arange(0,length)
        wn = 0.5 * (1-np.cos((2.0*np.pi*n)/(length+1)))
        filt = np.fft.fftshift(wn)*self._ramp(length)
        return filt / np.max(filt)

def zero_pad_2(array, exp):
    length = 2**exp
    length_to_add = 2**exp - len(array)
    assert length_to_add > 0, "Cannot zero pad to a smaller array size."
    return np.concatenate((array, np.zeros(length_to_add)))