import rotate_im as crot
import numpy as np

def rotate_image(angle, image):
    return crot.rotate_image(angle, np.float64(image))