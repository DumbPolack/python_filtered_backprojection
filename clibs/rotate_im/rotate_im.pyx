#Author: John A. Krol

import cython
import numpy as np
cimport numpy as np
cimport libc.math as cm

def rotate_image(double angle_deg, double[:,:] image):
    cdef int sizex = image.shape[0]
    cdef int sizey = image.shape[1]
    cdef np.ndarray[double, ndim=2] rotated = np.zeros([sizex,sizey],dtype=np.dtype('d'))
    cdef double xp, yp
    cdef int xit, yit, channel
    for xit in range(0, sizex - 1):
        for yit in range(0, sizey - 1):
            xp = _rotate_x_coord(angle_deg, xit, yit, sizex/2, sizey/2)
            yp = _rotate_y_coord(angle_deg, xit, yit, sizex/2, sizey/2)
            rotated[xit,yit] = _bilinear_interpolation(xp, yp, image, sizex, sizey)
    return rotated

cpdef _rotate_x_coord(double angle_deg, double x, double y, double xmid, double ymid):
    cdef double angle_rad = angle_deg * (cm.M_PI / 180.0)
    cdef double xp = (x - xmid) * cm.cos(angle_rad) + (y - ymid) * cm.sin(angle_rad) + xmid
    return xp

cpdef _rotate_y_coord(double angle_deg, double x, double y, double xmid, double ymid):
    cdef double angle_rad = angle_deg * (cm.M_PI / 180.0)
    cdef double yp = -(x - xmid) * cm.sin(angle_rad) + (y - ymid) * cm.cos(angle_rad) + ymid
    return yp

cpdef _bilinear_interpolation(double x, double y, double[:,:] image, int sizex, int sizey):
    cdef double x1 = cm.floor(x)
    if x1 < 0:
        x1 = 0
    elif x1 > sizex-2:
        x1 = sizex-2

    cdef double y1 = cm.floor(y)
    if y1 < 0:
        y1 = 0
    elif y1 > sizey-2:
        y1 = sizey-2

    cdef double x2 = x1+1
    cdef double y2 = y1+1
    cdef int x1i = <int>x1
    cdef int x2i = <int>x2
    cdef int y1i = <int>y1
    cdef int y2i = <int>y2

    cdef double fxy1 = image[x1i,y1i] * (x2 - x) / (x2 - x1) \
                       + image[x2i,y1i] * (x - x1) / (x2 - x1)
    cdef double fxy2 = image[x1i,y2i] * (x2 - x) / (x2 - x1) \
                       + image[x2i,y2i] * (x - x1) / (x2 - x1)

    cdef double bli = fxy1 * (y2 - y) / (y2 - y1) + fxy2 * (y - y1) / (y2 - y1)

    return bli