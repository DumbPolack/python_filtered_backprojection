#Author: John A. Krol

import numpy as np
import im_rotate
import projection
try:
    from clibs.rotate_im import c_rotate_im as c_im_rotate
except:
    print('Warning: C libraries not compiled! Run setup.py!')

class Loaded_Image():
    def __init__(self, image, name="my_image", use_clibs=False):
        #Image info:
        self.sizex = np.size(image, 0)
        self.sizey = np.size(image, 1)
        self.channels = np.size(image, 2)
        #Image containers:
        self.original = image
        self.intensity = None
        self.image = self.original
        self.name = name
        self.unscaled_sinogram = None
        self.filtered_unscaled_sinogram = None
        self.sinogram = None
        self.filtered_sinogram = None
        self.unscaled_backprojected = None
        self.backprojected = None
        self.to_intensity()
        #Projection profile:
        self.Projection = None
        self.use_clibs=use_clibs

    def rename(self, new_name):
        self.name = new_name

    def to_intensity(self):
        self.intensity = np.uint8(np.dot(self.image[...,:3], [0.299, 0.587, 0.114]))

    def revert(self):
        self.image = self.original

    def create_projection_profile(self, num_projections, angle_min, angle_max, filter_type, use_clibs):
        self.Projection = projection.Projection(self.sizex, self.sizey, num_projections, angle_min, angle_max, filter_type=filter_type, use_clibs=use_clibs)

    def rotate(self, angle):
        if self.use_clibs:
            self.image = np.uint8(c_im_rotate.rotate_image(angle, self.intensity))
        else:
            self.image = im_rotate.rotate_image(angle, self.intensity, sizex=self.sizex, sizey=self.sizey)

    def forward_project(self):
        self.unscaled_sinogram = projection.forward_project(self.intensity, self.Projection)
        shift = self.unscaled_sinogram - np.min(self.unscaled_sinogram)
        self.sinogram = np.uint8(255.0 * shift / np.max(shift))

    def filter_sinogram(self):
        self.filtered_unscaled_sinogram = projection.filter_sinogram(self.unscaled_sinogram, self.Projection)
        shift = self.filtered_unscaled_sinogram - np.min(self.filtered_unscaled_sinogram)
        self.filtered_sinogram = np.uint8(255.0 * shift / np.max(shift))

    def back_project(self):
        self.unscaled_backprojected = projection.back_project(self.filtered_unscaled_sinogram, self.Projection)
        shift = self.unscaled_backprojected - np.min(self.unscaled_backprojected)
        self.backprojected = np.uint8(255.0 * shift / np.max(shift))