#Author: John A. Krol

import numpy as np
import im_rotate
import filters as filt
try:
    from clibs.rotate_im import c_rotate_im as c_im_rotate
except:
    print('Warning: C libraries not compiled! Run setup.py!')

class Projection():
    sizex = None
    sizey = None
    num_projections = None
    angle_min = None
    angle_max = None
    filter_type = 2

    def __init__(self, sizex, sizey, num_projections, angle_min, angle_max, filter_type=2, use_clibs=False):
        self.sizex = sizex
        self.sizey = sizey
        self.num_projections = num_projections
        self.angle_min = angle_min
        self.angle_max = angle_max
        self.use_clibs = use_clibs
        self.filter_type=filter_type

def forward_project(image, Projection):
    step = (Projection.angle_max-Projection.angle_min)/Projection.num_projections
    sinogram = np.zeros([np.size(image,0), Projection.num_projections])
    for proj in range(Projection.num_projections):
        if Projection.use_clibs:
            sinogram[:,proj] = _lin_project(c_im_rotate.rotate_image(proj * step, image))
        else:
            sinogram[:, proj] = _lin_project(im_rotate.rotate_image(proj * step, image))
    return sinogram

def back_project(sinogram, Projection):
    back = np.zeros([Projection.sizex,Projection.sizey])
    step = (Projection.angle_max - Projection.angle_min) / Projection.num_projections
    for bproj in range(Projection.num_projections):
        if Projection.use_clibs:
            back = back + c_im_rotate.rotate_image(-step * bproj, np.tile(sinogram[:,bproj], [Projection.sizey,1]))
        else:
            back = back + im_rotate.rotate_image(-step * bproj, np.tile(sinogram[:, bproj], [Projection.sizey, 1]),
                                                 sizex=Projection.sizex, sizey=Projection.sizey)
    return back

def filter_sinogram(sinogram, Projection):
    rtn_sino = np.float64(sinogram)
    pwr = int(np.floor(np.log(Projection.sizex)/np.log(2)) + 1)
    Filter = filt.Filter(2 ** pwr, filter_type=Projection.filter_type)
    fn = Filter.filter
    for proj in range(Projection.num_projections):
        filtered_sino = np.real(np.fft.ifft(fn*np.fft.fft(filt.zero_pad_2(rtn_sino[:, proj], pwr))))
        rtn_sino[:, proj] = filtered_sino[0:Projection.sizex]
    return rtn_sino

def _lin_project(image):
    raw = np.sum(image, 0)
    return raw