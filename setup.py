#Author: John A. Krol

from distutils.core import setup
from Cython.Build import cythonize
import numpy
import pip
import os

#Cython builder
setup(
    ext_modules=cythonize(os.path.join('.','clibs','rotate_im','rotate_im.pyx')),
    include_dirs=[numpy.get_include()]
)

#pkg installer
def install(pkg):
    pip.main(['install', pkg])

if __name__ == '__main__':
    install('imageio')
    install('matplotlib')
    install('cython')